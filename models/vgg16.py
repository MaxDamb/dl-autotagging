import tensorflow as tf

from models.util import get_dim_and_horizontal_config, get_conv_layer_by_dim, InputSize, get_pool_layer_by_dim

big_kernels = [5, 4, 3, 2, 2]
medium_kernels = [(1,5), (2, 4), 3, 2, 2]
small_kernels = [(1,5), (1,4), (1,3), 2, 2]
one_dim_kernels = [5, 5, 5, 4, 4]


def build_vgg16(input_shape, num_classes, dropout_rate, l2_kernel_regularizer=None, batch_norm=True):
    input_size, input_dim = get_dim_and_horizontal_config(input_shape)

    cnn_model = tf.keras.Sequential([
        tf.keras.layers.BatchNormalization() if batch_norm and input_dim == 1 else tf.keras.layers.Layer(),
        get_conv_layer_by_dim(32, input_dim, 5, kernel_regularizer=l2_kernel_regularizer, padding='same')
            if input_dim == 1 else tf.keras.layers.Layer(),
        get_pool_layer_by_dim(input_dim, pool_size=5)
            if input_dim == 1 else tf.keras.layers.Layer(),

        tf.keras.layers.BatchNormalization() if batch_norm and input_dim == 1 else tf.keras.layers.Layer(),
        get_conv_layer_by_dim(32, input_dim, 5, kernel_regularizer=l2_kernel_regularizer, padding='same')
        if input_dim == 1 else tf.keras.layers.Layer(),
        get_pool_layer_by_dim(input_dim, pool_size=5)
        if input_dim == 1 else tf.keras.layers.Layer(),

        tf.keras.layers.BatchNormalization() if batch_norm else tf.keras.layers.Layer(),
        get_conv_layer_by_dim(64, input_dim, 3, kernel_regularizer=l2_kernel_regularizer, padding='same'),
        get_conv_layer_by_dim(64, input_dim, 3, kernel_regularizer=l2_kernel_regularizer, padding='same'),
        tf.keras.layers.Dropout(
            dropout_rate) if dropout_rate is not None and dropout_rate > 0.0 else tf.keras.layers.Layer(),

        get_pool_layer_by_dim(input_dim, pool_size=get_poolsize_for_vgg_lvl(input_dim, input_size, 0)),
        tf.keras.layers.BatchNormalization() if batch_norm else tf.keras.layers.Layer(),
        get_conv_layer_by_dim(128, input_dim, 3, kernel_regularizer=l2_kernel_regularizer, padding='same'),
        get_conv_layer_by_dim(128, input_dim, 3, kernel_regularizer=l2_kernel_regularizer, padding='same'),
        tf.keras.layers.Dropout(
            dropout_rate) if dropout_rate is not None and dropout_rate > 0.0 else tf.keras.layers.Layer(),

        get_pool_layer_by_dim(input_dim, pool_size=get_poolsize_for_vgg_lvl(input_dim, input_size, 1)),
        tf.keras.layers.BatchNormalization() if batch_norm else tf.keras.layers.Layer(),
        get_conv_layer_by_dim(256, input_dim, 3, kernel_regularizer=l2_kernel_regularizer, padding='same'),
        get_conv_layer_by_dim(256, input_dim, 3, kernel_regularizer=l2_kernel_regularizer, padding='same'),
        get_conv_layer_by_dim(256, input_dim, 3, kernel_regularizer=l2_kernel_regularizer, padding='same'),
        tf.keras.layers.Dropout(
            dropout_rate) if dropout_rate is not None and dropout_rate > 0.0 else tf.keras.layers.Layer(),

        get_pool_layer_by_dim(input_dim, pool_size=get_poolsize_for_vgg_lvl(input_dim, input_size, 2)),
        tf.keras.layers.BatchNormalization() if batch_norm else tf.keras.layers.Layer(),
        get_conv_layer_by_dim(512, input_dim, 3, kernel_regularizer=l2_kernel_regularizer, padding='same'),
        get_conv_layer_by_dim(512, input_dim, 3, kernel_regularizer=l2_kernel_regularizer, padding='same'),
        get_conv_layer_by_dim(512, input_dim, 3, kernel_regularizer=l2_kernel_regularizer, padding='same'),
        tf.keras.layers.Dropout(
            dropout_rate) if dropout_rate is not None and dropout_rate > 0.0 else tf.keras.layers.Layer(),

        get_pool_layer_by_dim(input_dim, pool_size=get_poolsize_for_vgg_lvl(input_dim, input_size, 3)),
        tf.keras.layers.BatchNormalization() if batch_norm else tf.keras.layers.Layer(),
        get_conv_layer_by_dim(512, input_dim, 3, kernel_regularizer=l2_kernel_regularizer, padding='same'),
        get_conv_layer_by_dim(512, input_dim, 3, kernel_regularizer=l2_kernel_regularizer, padding='same'),
        get_conv_layer_by_dim(512, input_dim, 3, kernel_regularizer=l2_kernel_regularizer, padding='same'),
        tf.keras.layers.Dropout(
            dropout_rate) if dropout_rate is not None and dropout_rate > 0.0 else tf.keras.layers.Layer(),

        get_pool_layer_by_dim(input_dim, pool_size=get_poolsize_for_vgg_lvl(input_dim, input_size, 4)),
        tf.keras.layers.BatchNormalization() if batch_norm else tf.keras.layers.Layer(),
        tf.keras.layers.Dropout(
            dropout_rate) if dropout_rate is not None and dropout_rate > 0.0 else tf.keras.layers.Layer(),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(4096, activation=tf.nn.relu),
        tf.keras.layers.Dropout(
            dropout_rate) if dropout_rate is not None and dropout_rate > 0.0 else tf.keras.layers.Layer(),
        tf.keras.layers.Dense(4096, activation=tf.nn.relu),
        tf.keras.layers.Dropout(
            dropout_rate) if dropout_rate is not None and dropout_rate > 0.0 else tf.keras.layers.Layer(),

        tf.keras.layers.Dense(num_classes),
        tf.keras.layers.Activation(activation=tf.nn.sigmoid, dynamic=tf.float32)
    ])

    return cnn_model


def get_poolsize_for_vgg_lvl(input_dim, input_size, lvl):
    if input_dim == 1:
        return one_dim_kernels[lvl]
    else:
        if input_size == InputSize.BIG_INPUT:
            return big_kernels[lvl]
        elif input_size == InputSize.MEDIUM_INPUT:
            return medium_kernels[lvl]
        else:
            return small_kernels[lvl]
