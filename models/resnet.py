import tensorflow as tf
from tensorflow.keras.layers import (MaxPool1D)
from models.util import get_conv_layer_by_dim, get_dim_and_horizontal_config, InputSize, basic_cnn_block


def build_resnet101(input_shape, num_classes, dropout=None, l2_kernel_regularizer=None, batch_norm=True):
    input_size, input_dim = get_dim_and_horizontal_config(input_shape)
    model = ResNet(layer_params=[3, 4, 6, 3], num_classes=num_classes, dim=input_dim, dropout_rate=dropout,
                   kernel_regularizer=l2_kernel_regularizer, batch_norm=batch_norm, input_size=input_size)
    return model


class ResNet(tf.keras.Model):
    def __init__(self, layer_params, num_classes=50, dim=2, input_size=InputSize.MEDIUM_INPUT, dropout_rate=None,
                 kernel_regularizer=None, batch_norm=True):
        super(ResNet, self).__init__()
        self.batch_norm = batch_norm
        self.dim = dim
        self.input_size = input_size

        if dim == 1:
            self.waveform_frontend = get_waveform_frontend(kernel_regularizer=kernel_regularizer, batch_norm=batch_norm)
        elif input_size == InputSize.BIG_INPUT:
            self.conv_block1 = basic_cnn_block(num_features=64,
                                               dim=dim,
                                               kernel_size=7,
                                               pool_size=6,
                                               batch_norm=batch_norm,
                                               dropout_rate=dropout_rate,
                                               l2_kernel_regularizer=kernel_regularizer)
        else:
            self.conv_block1 = basic_cnn_block(num_features=64,
                                               dim=dim,
                                               kernel_size=7,
                                               strides=2,
                                               padding='same',
                                               l2_kernel_regularizer=kernel_regularizer,
                                               batch_norm=batch_norm,
                                               pool_size=(2 if input_size == InputSize.MEDIUM_INPUT else 1, 7),
                                               dropout_rate=dropout_rate)

        self.layer1 = make_bottleneck_layer(filter_num=64,
                                            blocks=layer_params[0],
                                            dim=dim,
                                            dropout_rate=dropout_rate,
                                            kernel_regularizer=kernel_regularizer,
                                            batch_norm=batch_norm)
        self.layer2 = make_bottleneck_layer(filter_num=128,
                                            blocks=layer_params[1],
                                            stride=2,
                                            dim=dim,
                                            dropout_rate=dropout_rate,
                                            kernel_regularizer=kernel_regularizer,
                                            batch_norm=batch_norm)
        self.layer3 = make_bottleneck_layer(filter_num=256,
                                            blocks=layer_params[2],
                                            stride=2,
                                            dim=dim,
                                            dropout_rate=dropout_rate,
                                            kernel_regularizer=kernel_regularizer,
                                            batch_norm=batch_norm)
        self.layer4 = make_bottleneck_layer(filter_num=512,
                                            blocks=layer_params[3],
                                            stride=2,
                                            dim=dim,
                                            dropout_rate=dropout_rate,
                                            kernel_regularizer=kernel_regularizer,
                                            batch_norm=batch_norm)

        self.avgpool = tf.keras.layers.GlobalAveragePooling1D() if dim == 1 else tf.keras.layers.GlobalAveragePooling2D()
        self.fc = tf.keras.layers.Dense(units=num_classes)
        self.fc_act = tf.keras.layers.Activation(activation=tf.keras.activations.sigmoid, dtype=tf.float32)

    def call(self, inputs, training=None, mask=None):
        if self.dim == 1:
            x = self.waveform_frontend(inputs)
        else:
            x = self.conv_block1(inputs)
        x = self.layer1(x, training=training)
        x = self.layer2(x, training=training)
        x = self.layer3(x, training=training)
        x = self.layer4(x, training=training)
        x = self.avgpool(x)
        output = self.fc(x)
        output = self.fc_act(output)
        return output


def get_waveform_frontend(num_conv=6, kernel_regularizer=None, batch_norm=True):
    filters = [32, 64, 128, 256, 256, 512]
    seq = []
    for i in range(0, num_conv):
        seq.append(get_conv_layer_by_dim(filters=filters[i],
                                         dim=1,
                                         kernel_size=3,
                                         padding='same',
                                         kernel_regularizer=kernel_regularizer))
        seq.append(MaxPool1D(pool_size=2))
        if batch_norm:
            seq.append(tf.keras.layers.BatchNormalization())
    return tf.keras.Sequential(seq, name='waveform-frontend')


def make_bottleneck_layer(filter_num, blocks, stride=1, dim=2, dilation=1, dropout_rate=None,
                          kernel_regularizer=None, batch_norm=True):
    res_block = tf.keras.Sequential()
    res_block.add(BottleNeck(filter_num, stride=stride, dim=dim, dilation=dilation,
                             dropout_rate=dropout_rate, kernel_regularizer=kernel_regularizer, batch_norm=batch_norm))

    for _ in range(1, blocks):
        res_block.add(BottleNeck(filter_num, stride=1, dim=dim, dilation=dilation,
                                 dropout_rate=dropout_rate, kernel_regularizer=kernel_regularizer,
                                 batch_norm=batch_norm))

    return res_block


class BottleNeck(tf.keras.layers.Layer):
    def __init__(self, filter_num, stride=1, dim=2, dilation=1, dropout_rate=None,
                 kernel_regularizer=None, batch_norm=True):
        super(BottleNeck, self).__init__()
        self.stride = stride
        self.dropout_rate = dropout_rate
        self.batch_norm = batch_norm
        self.dim = dim
        self.conv1 = get_conv_layer_by_dim(filters=filter_num,
                                           dim=dim,
                                           kernel_size=1,
                                           strides=1,
                                           padding='same',
                                           kernel_regularizer=kernel_regularizer)
        if batch_norm:
            self.bn1 = tf.keras.layers.BatchNormalization()
        self.conv2 = get_conv_layer_by_dim(filters=filter_num,
                                           dim=dim,
                                           kernel_size=3,
                                           strides=stride,
                                           padding='same',
                                           dilation=dilation,
                                           kernel_regularizer=kernel_regularizer)
        if batch_norm:
            self.bn2 = tf.keras.layers.BatchNormalization()
        self.conv3 = get_conv_layer_by_dim(filters=filter_num * 4,
                                           dim=dim,
                                           kernel_size=1,
                                           strides=1,
                                           padding='same',
                                           kernel_regularizer=kernel_regularizer)
        if batch_norm:
            self.bn3 = tf.keras.layers.BatchNormalization()

        self.downsample = tf.keras.Sequential()
        self.downsample.add(get_conv_layer_by_dim(filters=filter_num * 4,
                                                  dim=dim,
                                                  kernel_size=1,
                                                  strides=stride,
                                                  kernel_regularizer=kernel_regularizer))
        if self.batch_norm:
            self.downsample.add(tf.keras.layers.BatchNormalization())
        if dropout_rate is not None:
            self.do = tf.keras.layers.Dropout(dropout_rate)

    def call(self, inputs, training=None, **kwargs):
        residual = self.downsample(inputs)

        x = self.conv1(inputs)
        if self.batch_norm:
            x = self.bn1(x, training=training)
        x = tf.nn.relu(x)
        x = self.conv2(x)
        if self.batch_norm:
            x = self.bn2(x, training=training)
        x = tf.nn.relu(x)
        x = self.conv3(x)
        if self.batch_norm:
            x = self.bn3(x, training=training)

        output = tf.nn.relu(tf.keras.layers.add([residual, x]))
        if self.dropout_rate is not None:
            output = self.do(output)

        return output
