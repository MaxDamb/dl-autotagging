import tensorflow as tf
from tensorflow.keras.layers import Flatten, Dropout, Dense, Conv2D
from tensorflow.keras.layers import (MaxPool1D, BatchNormalization, Activation, Input)
from tensorflow.python.keras.layers import MaxPool2D
from collections.abc import Iterable

from models.util import get_conv_layer_by_dim, get_dim_and_horizontal_config, InputSize, basic_cnn_block


def build_dilated_cnn(input_shape, num_classes, dropout, l2_kernel_regularizer=None, batch_norm=True,
                      num_filt_midend=64, num_units_backend=200):
    input_size, input_dim = get_dim_and_horizontal_config(input_shape)
    input = Input(shape=input_shape)
    if input_dim == 2:
        frontend_features = build_two_dimensional_frontend(batch_norm, input, input_dim, input_shape, input_size,
                                                           l2_kernel_regularizer)
    else:
        frontend_features = dcnn_waveform_frontend(input, 7, l2_kernel_regularizer, batch_norm)
    midend_features_list = midend(frontend_features, num_filt_midend, dropout_rate=dropout,
                                  l2_kernel_regularizer=l2_kernel_regularizer, batch_norm=batch_norm)
    midend_features = tf.concat(midend_features_list, 2)

    logits = backend(midend_features, num_classes, num_units_backend,
                     l2_kernel_regularizer=l2_kernel_regularizer, batch_norm=batch_norm)

    return tf.keras.Model(input, logits)


def build_two_dimensional_frontend(batch_norm, input, input_dim, input_shape, input_size, l2_kernel_regularizer):
    if input_size == InputSize.BIG_INPUT:
        frontend_input = basic_cnn_block(4, 2, kernel_size=2, strides=1, padding='valid', pool_size=(4, 1))(
            input)
        frontend_input = basic_cnn_block(8, 2, kernel_size=2, strides=1, padding='same', pool_size=(3, 1))(
            frontend_input)
    else:
        frontend_input = input
    frontend_features_list = dcnn_frontend(frontend_input, input_shape[0], num_filt=1.6, input_dim=input_dim,
                                           kernel_regularizer=l2_kernel_regularizer, batch_norm=batch_norm)
    frontend_features = tf.concat(frontend_features_list, 2)
    return frontend_features


def dcnn_waveform_frontend(x, num_conv=7, kernel_regularizer=None, batch_norm=True):
    filters = [64, 64, 128, 128, 256, 256, 256, 512]
    for i in range(0, num_conv):
        x = dcnn_waveform_block(x, int(filters[i] / 2), kernel_regularizer, batch_norm)
    return x


def dcnn_waveform_block(x, filters, kernel_regularizer, batch_norm):
    c1 = get_conv_layer_by_dim(filters, 1, 3, kernel_regularizer=kernel_regularizer, padding='same', dilation=1)(x)
    c2 = get_conv_layer_by_dim(filters, 1, 3, kernel_regularizer=kernel_regularizer, padding='same', dilation=2)(x)
    c3 = get_conv_layer_by_dim(filters, 1, 3, kernel_regularizer=kernel_regularizer, padding='same', dilation=3)(x)
    c5 = get_conv_layer_by_dim(filters, 1, 3, kernel_regularizer=kernel_regularizer, padding='same', dilation=5)(x)

    block = tf.concat([c1, c2, c3, c5], 2)

    c1 = musicnn_block(block, filters, 3, 'same', 1, 1, kernel_regularizer, batch_norm)
    c2 = musicnn_block(block, filters, 3, 'same', 1, 2, kernel_regularizer, batch_norm)
    c3 = musicnn_block(block, filters, 3, 'same', 1, 3, kernel_regularizer, batch_norm)
    c5 = musicnn_block(block, filters, 3, 'same', 1, 5, kernel_regularizer, batch_norm)

    return tf.concat([c1, c2, c3, c5], 2)


def dcnn_frontend(x, yInput, num_filt, input_dim, kernel_regularizer=None, batch_norm=True):
    if batch_norm:
        x = BatchNormalization()(x)
    kernel_size = [1, 8]

    sd1 = get_conv_layer_by_dim(num_filt * 16, 2, kernel_size, 1,
                                kernel_regularizer=kernel_regularizer, padding='same', dilation=1)(x)
    sd4 = get_conv_layer_by_dim(num_filt * 16, 2, kernel_size, 1,
                                kernel_regularizer=kernel_regularizer, padding='same', dilation=4)(x)
    sd7 = get_conv_layer_by_dim(num_filt * 16, 2, kernel_size, 1,
                                kernel_regularizer=kernel_regularizer, padding='same', dilation=7)(x)
    s_stacked = tf.concat([sd1, sd4, sd7], 3)

    sd1 = musicnn_block(inputs=s_stacked,
                        padding='same',
                        filters=int(num_filt * 32),
                        kernel_size=kernel_size,
                        input_dim=input_dim,
                        batch_norm=batch_norm,
                        kernel_regularizer=kernel_regularizer,
                        dilation=1)
    sd4 = musicnn_block(inputs=s_stacked,
                        padding='same',
                        filters=int(num_filt * 32),
                        kernel_size=kernel_size,
                        input_dim=input_dim,
                        batch_norm=batch_norm,
                        kernel_regularizer=kernel_regularizer,
                        dilation=4)
    sd7 = musicnn_block(inputs=s_stacked,
                        padding='same',
                        filters=int(num_filt * 32),
                        kernel_size=kernel_size,
                        input_dim=input_dim,
                        batch_norm=batch_norm,
                        kernel_regularizer=kernel_regularizer,
                        dilation=7)

    kernel_size = [5, 2]

    fd1 = get_conv_layer_by_dim(num_filt * 16, 2, kernel_size, 1,
                                kernel_regularizer=kernel_regularizer, padding='same', dilation=1)(x)
    fd2 = get_conv_layer_by_dim(num_filt * 16, 2, kernel_size, 1,
                                kernel_regularizer=kernel_regularizer, padding='same', dilation=2)(x)
    fd3 = get_conv_layer_by_dim(num_filt * 16, 2, kernel_size, 1,
                                kernel_regularizer=kernel_regularizer, padding='same', dilation=3)(x)
    fd5 = get_conv_layer_by_dim(num_filt * 16, 2, kernel_size, 1,
                                kernel_regularizer=kernel_regularizer, padding='same', dilation=5)(x)

    f_stacked = tf.concat([fd1, fd2, fd3, fd5], 3)

    fd1 = musicnn_block(inputs=f_stacked,
                        padding='same',
                        filters=int(num_filt * 32),
                        kernel_size=kernel_size,
                        input_dim=input_dim,
                        batch_norm=batch_norm,
                        kernel_regularizer=kernel_regularizer,
                        dilation=1)
    fd2 = musicnn_block(inputs=f_stacked,
                        padding='same',
                        filters=int(num_filt * 32),
                        kernel_size=kernel_size,
                        input_dim=input_dim,
                        batch_norm=batch_norm,
                        kernel_regularizer=kernel_regularizer,
                        dilation=2)
    fd3 = musicnn_block(inputs=f_stacked,
                        padding='same',
                        filters=int(num_filt * 32),
                        kernel_size=kernel_size,
                        input_dim=input_dim,
                        batch_norm=batch_norm,
                        kernel_regularizer=kernel_regularizer,
                        dilation=3)
    fd5 = musicnn_block(inputs=f_stacked,
                        padding='same',
                        filters=int(num_filt * 32),
                        kernel_size=kernel_size,
                        input_dim=input_dim,
                        batch_norm=batch_norm,
                        kernel_regularizer=kernel_regularizer,
                        dilation=5)

    return [sd1, sd4, sd7, fd1, fd2, fd3, fd5]


def midend(front_end_output, num_filt, dropout_rate=None, l2_kernel_regularizer=None, batch_norm=True):
    front_end_output = tf.expand_dims(front_end_output, 3)
    front_end_pad = tf.pad(front_end_output, [[0, 0], [3, 3], [0, 0], [0, 0]], "CONSTANT")

    if dropout_rate is not None:
        front_end_pad = tf.keras.layers.Dropout(dropout_rate)(front_end_pad)
    conv1 = Conv2D(filters=num_filt,
                   kernel_size=[7, front_end_pad.shape[2]],
                   padding="valid",
                   activation=tf.nn.relu,
                   kernel_regularizer=l2_kernel_regularizer)(front_end_pad)
    if batch_norm:
        conv1 = BatchNormalization()(conv1)
    bn_conv1_t = tf.transpose(conv1, [0, 1, 3, 2])
    bn_conv1_pad = tf.pad(bn_conv1_t, [[0, 0], [3, 3], [0, 0], [0, 0]], "CONSTANT")

    if dropout_rate is not None:
        bn_conv1_pad = tf.keras.layers.Dropout(dropout_rate)(bn_conv1_pad)
    conv2 = Conv2D(filters=num_filt,
                   kernel_size=[7, bn_conv1_pad.shape[2]],
                   padding="valid",
                   activation=tf.nn.relu,
                   kernel_regularizer=l2_kernel_regularizer)(bn_conv1_pad)
    if batch_norm:
        conv2 = BatchNormalization()(conv2)
    conv2 = tf.transpose(conv2, [0, 1, 3, 2])
    res_conv2 = tf.add(conv2, bn_conv1_t)
    bn_conv2_pad = tf.pad(res_conv2, [[0, 0], [3, 3], [0, 0], [0, 0]], "CONSTANT")

    if dropout_rate is not None:
        bn_conv2_pad = tf.keras.layers.Dropout(dropout_rate)(bn_conv2_pad)
    conv3 = Conv2D(filters=num_filt,
                   kernel_size=[7, bn_conv2_pad.shape[2]],
                   padding="valid",
                   activation=tf.nn.relu,
                   kernel_regularizer=l2_kernel_regularizer)(bn_conv2_pad)
    if batch_norm:
        conv3 = BatchNormalization()(conv3)
    conv3 = tf.transpose(conv3, [0, 1, 3, 2])
    res_conv3 = tf.add(conv3, res_conv2)

    return [front_end_output, bn_conv1_t, res_conv2, res_conv3]


def backend(feature_map, num_classes, output_units, l2_kernel_regularizer=None, batch_norm=True):
    # temporal pooling
    max_pool = tf.reduce_max(feature_map, axis=1)
    mean_pool, var_pool = tf.nn.moments(feature_map, axes=[1])
    tmp_pool = tf.concat([max_pool, mean_pool], 2)

    # penultimate dense layer
    flat_pool = Flatten()(tmp_pool)
    if batch_norm:
        flat_pool = BatchNormalization()(flat_pool)
    flat_pool_dropout = Dropout(rate=0.5)(flat_pool)
    dense = Dense(units=output_units,
                  activation=tf.nn.relu,
                  kernel_regularizer=l2_kernel_regularizer)(flat_pool_dropout)
    if batch_norm:
        dense = BatchNormalization()(dense)
    dense_dropout = Dropout(rate=0.5)(dense)

    # output dense layer
    fc = tf.keras.layers.Dense(units=num_classes)(dense_dropout)
    logits = tf.keras.layers.Activation(activation=tf.keras.activations.sigmoid, dtype=tf.float32)(fc)

    return logits


def musicnn_block(inputs, filters, kernel_size, padding="valid", input_dim=2, dilation=1, kernel_regularizer=None,
                  batch_norm=True):
    if input_dim == 1:
        padding = 'same'
        if isinstance(kernel_size, Iterable):
            kernel_size = max(kernel_size)
    conv = get_conv_layer_by_dim(filters, input_dim, kernel_size, padding=padding, dilation=dilation,
                                 kernel_regularizer=kernel_regularizer)(inputs)
    pool = MaxPool1D(pool_size=3)(conv) if input_dim == 1 \
        else MaxPool2D(pool_size=[1, conv.shape[2]], strides=[1, conv.shape[2]])(conv)
    if batch_norm:
        pool = BatchNormalization()(pool)
    if input_dim == 1:
        return pool
    else:
        return tf.squeeze(pool, [2])
