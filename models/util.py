from enum import Enum

import tensorflow as tf
from tensorflow.keras.layers import (Conv1D, MaxPool1D, BatchNormalization, Activation)
from tensorflow.python.keras.layers import Conv2D, MaxPool2D


class InputSize(Enum):
    BIG_INPUT = 0
    MEDIUM_INPUT = 1
    SMALL_INPUT = 2


def get_dim_and_horizontal_config(shape):
    apply_horizontal_pool = InputSize.BIG_INPUT
    dim = 2
    if len(shape) == 2:
        dim = 1
    elif shape[0] < 32:
        apply_horizontal_pool = InputSize.SMALL_INPUT
    elif shape[0] < 512:
        apply_horizontal_pool = InputSize.MEDIUM_INPUT
    return apply_horizontal_pool, dim


def get_conv_layer_by_dim(filters, dim: int, kernel_size=3, strides=1, kernel_regularizer=None, padding='valid',
                          dilation=1):
    if dim == 1:
        return Conv1D(filters, kernel_size=kernel_size, strides=strides, padding=padding,
                      kernel_regularizer=kernel_regularizer, dilation_rate=dilation)
    elif dim == 2:
        return Conv2D(filters, kernel_size=kernel_size, strides=strides, padding=padding,
                      kernel_regularizer=kernel_regularizer, dilation_rate=dilation)
    else:
        raise ValueError(f'Unsupported dimension {dim}')


def get_pool_layer_by_dim(dim: int, pool_size=2, strides=None, padding='valid'):
    if dim == 1:
        return MaxPool1D(pool_size=pool_size, strides=strides, padding=padding)
    elif dim == 2:
        return MaxPool2D(pool_size=pool_size, strides=strides, padding=padding)
    else:
        raise ValueError(f'Unsupported dimension {dim}')


def basic_cnn_block(num_features, dim, kernel_size=3, strides=1, padding='same', pool_size=(2, 2),
                    dropout_rate=None, l2_kernel_regularizer=None, batch_norm=True):
    layers = []
    layers.append(get_conv_layer_by_dim(num_features, dim, kernel_size=kernel_size,
                                        strides=strides, padding=padding,
                                        kernel_regularizer=l2_kernel_regularizer))
    if batch_norm:
        layers.append(BatchNormalization())
    if dropout_rate is not None and dropout_rate > 0.0:
        layers.append(tf.keras.layers.Dropout(dropout_rate))
    layers.append(Activation('relu'))
    layers.append(MaxPool1D(pool_size=pool_size) if dim == 1 else MaxPool2D(pool_size=pool_size))
    return tf.keras.Sequential(layers)
