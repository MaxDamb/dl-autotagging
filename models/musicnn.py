import tensorflow as tf
from tensorflow.keras.layers import (MaxPool1D, BatchNormalization, Dense, Dropout, Input)
from tensorflow.python.keras.layers import Conv2D, MaxPool2D, Flatten
from collections.abc import Iterable

from models.util import get_dim_and_horizontal_config, get_conv_layer_by_dim, InputSize, basic_cnn_block


def build_musicnn(input_shape, num_classes, dropout_rate=None, l2_kernel_regularizer=None, batch_norm=True, num_filt_midend=64, num_units_backend=200):
    input_size, input_dim = get_dim_and_horizontal_config(input_shape)

    input = Input(shape=input_shape)

    if input_dim == 2:
        frontend_features = build_two_dimensional_frontend(batch_norm, input, input_dim, input_shape, input_size,
                                                           l2_kernel_regularizer)
    else:
        frontend_features = build_waveform_frontend(input, 7, kernel_regularizer=l2_kernel_regularizer, batch_norm=batch_norm)

    midend_features_list = midend(frontend_features, num_filt_midend, dropout_rate=dropout_rate,
                            l2_kernel_regularizer=l2_kernel_regularizer, batch_norm=batch_norm)
    midend_features = tf.concat(midend_features_list, 2)

    logits = backend(midend_features, num_classes, num_units_backend,
                            l2_kernel_regularizer=l2_kernel_regularizer, batch_norm=batch_norm)

    return tf.keras.Model(input, logits)


def build_two_dimensional_frontend(batch_norm, input, input_dim, input_shape, input_size, l2_kernel_regularizer):
    if input_size == InputSize.BIG_INPUT:
        frontend_input = MaxPool2D((2, 1))(input)
        frontend_input = basic_cnn_block(4, 2, kernel_size=2, strides=1, padding='same', pool_size=(4, 1))(
            frontend_input)
        frontend_input = basic_cnn_block(8, 2, kernel_size=2, strides=1, padding='same', pool_size=(3, 1))(
            frontend_input)
    else:
        frontend_input = input
    frontend_features_list = frontend(frontend_input, input_shape[0], num_filt=1.6, input_dim=input_dim,
                                      kernel_regularizer=l2_kernel_regularizer, batch_norm=batch_norm)
    frontend_features = tf.concat(frontend_features_list, 2)
    return frontend_features


def frontend(x, yInput, num_filt, input_dim, kernel_regularizer=None, batch_norm=True):
    if batch_norm:
        x = BatchNormalization()(x)

    # TIMBRAL-Blocks
    kernel_size = int(0.0004 * yInput) if input_dim == 1 else [7, int(0.4 * yInput)]
    f74 = musicnn_block(inputs=x,
                        padding='same',
                        filters=int(num_filt * 128),
                        kernel_size=kernel_size, input_dim=input_dim,
                        batch_norm=batch_norm,
                        kernel_regularizer=kernel_regularizer)
    kernel_size = int(0.0007 * yInput) if input_dim == 1 else [7, int(0.7 * yInput)]
    f77 = musicnn_block(inputs=x,
                        padding='same',
                        filters=int(num_filt * 128),
                        kernel_size=kernel_size, input_dim=input_dim,
                        batch_norm=batch_norm,
                        kernel_regularizer=kernel_regularizer)

    # TEMPO-Blocks
    s1 = musicnn_block(inputs=x,
                       padding='same',
                       filters=int(num_filt * 32),
                       kernel_size=[128, 1], input_dim=input_dim,
                        batch_norm=batch_norm,
                        kernel_regularizer=kernel_regularizer)

    s2 = musicnn_block(inputs=x,
                       padding='same',
                       filters=int(num_filt * 32),
                       kernel_size=[64, 1], input_dim=input_dim,
                        batch_norm=batch_norm,
                        kernel_regularizer=kernel_regularizer)

    s3 = musicnn_block(inputs=x,
                       padding='same',
                       filters=int(num_filt * 32),
                       kernel_size=[32, 1], input_dim=input_dim,
                        batch_norm=batch_norm,
                        kernel_regularizer=kernel_regularizer)

    return [f74, f77, s1, s2, s3]


def build_waveform_frontend(x, num_conv=7, kernel_regularizer=None, batch_norm=True):
    filters = [64, 64, 64, 128, 128, 128, 256, 256, 256]
    for i in range(0, num_conv):
        x = musicnn_block(x, filters[i], 3, 'same', 1, 1, kernel_regularizer, batch_norm)
    return x


def midend(front_end_output, num_filt, dropout_rate=None, l2_kernel_regularizer=None, batch_norm=True):
    front_end_output = tf.expand_dims(front_end_output, 3)
    front_end_pad = tf.pad(front_end_output, [[0, 0], [3, 3], [0, 0], [0, 0]], "CONSTANT")

    if dropout_rate is not None:
        front_end_pad = tf.keras.layers.Dropout(dropout_rate)(front_end_pad)
    conv1 = Conv2D(filters=num_filt,
                   kernel_size=[7, front_end_pad.shape[2]],
                   padding="valid",
                   activation=tf.nn.relu,
                   kernel_regularizer=l2_kernel_regularizer)(front_end_pad)
    if batch_norm:
        conv1 = BatchNormalization()(conv1)
    bn_conv1_t = tf.transpose(conv1, [0, 1, 3, 2])
    bn_conv1_pad = tf.pad(bn_conv1_t, [[0, 0], [3, 3], [0, 0], [0, 0]], "CONSTANT")

    if dropout_rate is not None:
        bn_conv1_pad = tf.keras.layers.Dropout(dropout_rate)(bn_conv1_pad)
    conv2 = Conv2D(filters=num_filt,
                   kernel_size=[7, bn_conv1_pad.shape[2]],
                   padding="valid",
                   activation=tf.nn.relu,
                   kernel_regularizer=l2_kernel_regularizer)(bn_conv1_pad)
    if batch_norm:
        conv2 = BatchNormalization()(conv2)
    conv2 = tf.transpose(conv2, [0, 1, 3, 2])
    res_conv2 = tf.add(conv2, bn_conv1_t)
    bn_conv2_pad = tf.pad(res_conv2, [[0, 0], [3, 3], [0, 0], [0, 0]], "CONSTANT")

    if dropout_rate is not None:
        bn_conv2_pad = tf.keras.layers.Dropout(dropout_rate)(bn_conv2_pad)
    conv3 = Conv2D(filters=num_filt,
                   kernel_size=[7, bn_conv2_pad.shape[2]],
                   padding="valid",
                   activation=tf.nn.relu,
                   kernel_regularizer=l2_kernel_regularizer)(bn_conv2_pad)
    if batch_norm:
        conv3 = BatchNormalization()(conv3)
    conv3 = tf.transpose(conv3, [0, 1, 3, 2])
    res_conv3 = tf.add(conv3, res_conv2)

    return [front_end_output, bn_conv1_t, res_conv2, res_conv3]

def backend(feature_map, num_classes, output_units, l2_kernel_regularizer=None, batch_norm=True):
    # temporal pooling
    max_pool = tf.reduce_max(feature_map, axis=1)
    mean_pool, var_pool = tf.nn.moments(feature_map, axes=[1])
    tmp_pool = tf.concat([max_pool, mean_pool], 2)

    # penultimate dense layer
    flat_pool = Flatten()(tmp_pool)
    if batch_norm:
        flat_pool = BatchNormalization()(flat_pool)
    flat_pool_dropout = Dropout(rate=0.5)(flat_pool)
    dense = Dense(units=output_units,
                  activation=tf.nn.relu,
                  kernel_regularizer=l2_kernel_regularizer)(flat_pool_dropout)
    if batch_norm:
        dense = BatchNormalization()(dense)
    dense_dropout = Dropout(rate=0.5)(dense)

    # output dense layer
    fc = tf.keras.layers.Dense(units=num_classes)(dense_dropout)
    logits = tf.keras.layers.Activation(activation=tf.keras.activations.sigmoid, dtype=tf.float32)(fc)

    return logits


def musicnn_block(inputs, filters, kernel_size, padding="valid", input_dim=2, dilation=1, kernel_regularizer=None, batch_norm=True):
    if input_dim == 1:
        padding = 'same'
        if isinstance(kernel_size, Iterable):
            kernel_size = max(kernel_size)
    conv = get_conv_layer_by_dim(filters, input_dim, kernel_size, padding=padding, dilation=dilation,
                                 kernel_regularizer=kernel_regularizer)(inputs)
    pool = MaxPool1D(pool_size=2)(conv) if input_dim == 1 \
        else MaxPool2D(pool_size=[1, conv.shape[2]], strides=[1, conv.shape[2]])(conv)
    if batch_norm:
        pool = BatchNormalization()(pool)
    if input_dim == 1:
        return pool
    else:
        return tf.squeeze(pool, [2])