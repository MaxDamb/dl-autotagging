import tensorflow as tf

from models.dil_cnn import build_dilated_cnn
from models.musicnn import build_musicnn
from models.resnet import build_resnet101
from models.senet import build_senet
from models.vgg16 import build_vgg16


def get_model(model_name: str, nr_output_tags: int, batch_shape, dropout=None, l2_kernel_regularizer=None, batch_norm=True) -> tf.keras.Sequential:
    if dropout <= 0.0:
        dropout = None
    if l2_kernel_regularizer <= 0.0:
        l2_kernel_regularizer = None
    if l2_kernel_regularizer is not None:
        l2_kernel_regularizer = tf.keras.regularizers.L2(l2_kernel_regularizer)
    switcher = {
        'vgg16': lambda batch_shape, nr_output_tags: build_vgg16(batch_shape, num_classes=nr_output_tags,
                                                                         dropout_rate=dropout, l2_kernel_regularizer=l2_kernel_regularizer,
                                                                         batch_norm=batch_norm),
        'senet': lambda batch_shape, nr_output_tags: build_senet(batch_shape, num_classes=nr_output_tags,
                                                                         dropout_rate=dropout, l2_kernel_regularizer=l2_kernel_regularizer,
                                                                         batch_norm=batch_norm),
        'musicnn': lambda batch_shape, nr_output_tags: build_musicnn(batch_shape, num_classes=nr_output_tags,
                                                                         dropout_rate=dropout, l2_kernel_regularizer=l2_kernel_regularizer,
                                                                         batch_norm=batch_norm),
        'resnet': lambda batch_shape, nr_output_tags: build_resnet101(batch_shape, num_classes=nr_output_tags,
                                                                         dropout=dropout, l2_kernel_regularizer=l2_kernel_regularizer,
                                                                         batch_norm=batch_norm),
        'dil_cnn': lambda batch_shape, nr_output_tags: build_dilated_cnn(batch_shape, num_classes=nr_output_tags,
                                                                         dropout=dropout, l2_kernel_regularizer=l2_kernel_regularizer,
                                                                         batch_norm=batch_norm)
    }
    return switcher.get(model_name)(batch_shape, nr_output_tags)
