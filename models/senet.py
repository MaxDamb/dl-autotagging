import tensorflow as tf
from tensorflow.keras.layers import (BatchNormalization, GlobalAvgPool1D, Multiply, GlobalMaxPool1D,
                                     Dense, Dropout, Activation, Reshape, Input, Concatenate)
from tensorflow.python.keras.layers import GlobalMaxPool2D, GlobalAvgPool2D

from models.util import get_dim_and_horizontal_config, InputSize, basic_cnn_block


def build_senet(shape, init_features=128, amplifying_ratio=16, drop_rate=0.5, num_classes=50,
                dropout_rate=None, l2_kernel_regularizer=None, batch_norm=True):
    """Build a SampleCNN model.
    Args:
      shape: A tensor shape not including batch size (e.g. (59049,))
      init_features: Number of feature maps of the first convolution.
      amplifying_ratio: Amplifying ratio of SE (not used for res and basic).
      drop_rate: Dropout rate.
      num_classes: Number of classes to predict.
    Returns:
      Keras Model.
    """
    input_size, input_dim = get_dim_and_horizontal_config(shape)
    input = Input(shape=shape)

    x = basic_cnn_block(init_features, input_dim, 3, 1, 'same',
                        (2 if input_dim == 1 else (2, 1)) if input_size == InputSize.BIG_INPUT else 1,
                        dropout_rate, l2_kernel_regularizer, batch_norm)(input)
    if batch_norm:
        x = BatchNormalization()(x)
    x = Activation('relu')(x)

    num_features = init_features
    layer_outputs = []
    for i in range(9):
        append_senet_block(amplifying_ratio, batch_norm, dropout_rate, i, input_dim, input_size, l2_kernel_regularizer,
                           layer_outputs, num_features, x)

    x = Concatenate()(
        [GlobalMaxPool1D()(output) if input_dim == 1 else GlobalMaxPool2D()(output) for output in layer_outputs[-3:]])

    x = Dense(x.shape[-1], kernel_initializer='glorot_uniform')(x)
    if batch_norm:
        x = BatchNormalization()(x)
    x = Activation('relu')(x)

    if drop_rate > 0.0:
        x = Dropout(drop_rate)(x)

    x = Dense(num_classes * 3, kernel_initializer='glorot_uniform')(x)
    if batch_norm:
        x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = Dense(num_classes, kernel_initializer='glorot_uniform')(x)
    x = Activation(activation='sigmoid', dtype=tf.float32)(x)
    return tf.keras.Model(input, x)


def append_senet_block(amplifying_ratio, batch_norm, dropout_rate, i, input_dim, input_size, l2_kernel_regularizer,
                       layer_outputs, num_features, x):
    num_features *= 2 if (i == 2 or i == 8) else 1
    use_horizontal_pool = False
    # For small inputs use horizontal pooling for 2/3 layers, for medium inputs for 1/2 layers or dim = 1
    if i % 3 < 2 and input_size == InputSize.SMALL_INPUT or i % 2 == 0 and (
            input_size == InputSize.MEDIUM_INPUT or input_dim == 1):
        use_horizontal_pool = True
    x = build_squeeze_and_exitation_block(x, num_features, amplifying_ratio, input_dim, use_horizontal_pool,
                                          dropout_rate=dropout_rate,
                                          l2_kernel_regularizer=l2_kernel_regularizer, batch_norm=batch_norm)
    layer_outputs.append(x)


def build_squeeze_and_exitation_block(x, num_features, amplifying_ratio, dim, use_horizontal_pool=False,
                                      dropout_rate=None, l2_kernel_regularizer=None, batch_norm=True):
    if use_horizontal_pool:
        x = basic_cnn_block(num_features, dim=dim, pool_size=1 if dim == 1 else (1, 2),
                            dropout_rate=dropout_rate, l2_kernel_regularizer=l2_kernel_regularizer,
                            batch_norm=batch_norm)(x)
    else:
        x = basic_cnn_block(num_features, dim, pool_size=2,
                            dropout_rate=dropout_rate, l2_kernel_regularizer=l2_kernel_regularizer,
                            batch_norm=batch_norm)(x)
    x = Multiply()([x, se_fn(x, amplifying_ratio, dim)])
    return x


def se_fn(x, amplifying_ratio, dim):
    num_features = x.shape[-1]
    x = GlobalAvgPool1D()(x) if dim == 1 else GlobalAvgPool2D()(x)
    x = Reshape((1, num_features))(x)
    x = Dense(num_features * amplifying_ratio, activation='relu', kernel_initializer='glorot_uniform')(x)
    x = Dense(num_features, activation='sigmoid', kernel_initializer='glorot_uniform')(x)
    return x
