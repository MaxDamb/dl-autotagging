import datetime
import math
import os
import random

from tqdm import tqdm
import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa
import matplotlib.pyplot as plt

import env
import wandb

from batch_generator import BatchGenerator
from evaluation import EvaluationResult
from models.models import get_model
from preprocessing import MagnaTagATune, \
    PreprocessingMethod, Subset, Dataset, MtgJamendo
from run_configs.mtg_jamendo.dil_cnn import dil_cnn_configs
from run_configs.mtg_jamendo.musicnn import musicnn_configs
from run_configs.mtg_jamendo.resnet import resnet_configs
from run_configs.mtg_jamendo.senet import senet_configs
from run_configs.mtg_jamendo.vgg16 import vgg16_configs


def check_and_save_model(best_validation_score, cnn_model, model, pr, preprocessing_method, roc, dataset_name) -> float:
    models_path = 'trained_models'
    if (roc + pr) > best_validation_score:
        best_validation_score = roc + pr
        if not os.path.exists(models_path):
            os.mkdir(models_path)
        if not os.path.exists(os.path.join(models_path, dataset_name)):
            os.mkdir(os.path.join(models_path, dataset_name))
        if not os.path.exists(os.path.join(models_path, dataset_name, model)):
            os.mkdir(os.path.join(models_path, dataset_name, model))
        cnn_model.save_weights(os.path.join(models_path, dataset_name, model, preprocessing_method))
    return best_validation_score


def train_model(train_dataset, val_dataset, batch_size, preprocessing_method, model, model_name,
                optimizer: tf.optimizers.Optimizer, test_batch_size, epochs=10, loss_fn=tf.keras.losses.BinaryCrossentropy(),
                log_step=200,
                early_stopping_epochs=None, train_acc_metric=None, early_lr_stopping=None,
                second_optimizer: tf.optimizers.Optimizer = None, second_optimizer_epochs=None,
                parallel_input_processing=None) -> bool:
    train_batch_gen = BatchGenerator(train_dataset, batch_size, True, parallel_input_processing)
    valid_batch_gen = BatchGenerator(val_dataset, test_batch_size, False, parallel_input_processing)

    roc_history = []
    pr_history = []
    avg_loss_history = []
    best_validation_score = 0.0
    epoch_times = []
    epochs_count = 0
    for epoch in range(epochs):
        epochs_count += 1
        print("\nStart of epoch %d" % (epoch,))

        if epoch == second_optimizer_epochs:
            optimizer = second_optimizer
        train_loss = []
        start_time = datetime.datetime.now()

        # Iterate over the batches of the dataset
        # with tf.profiler.experimental.Profile('logs/'):
        for batch in tqdm(train_batch_gen):
            train_step_from_batch(batch, loss_fn, model, optimizer, train_acc_metric, train_loss)
            log_train_loss(avg_loss_history, log_step, train_loss)
            if check_for_bad_learning_rate_criteria(avg_loss_history, early_lr_stopping, train_batch_gen, train_loss):
                return False

        epoch_times.append((datetime.datetime.now() - start_time).total_seconds())
        train_acc = train_acc_metric.result()
        print("Training acc over epoch: %.4f" % (float(train_acc),))

        # Run a validation loop at the end of each epoch
        best_validation_score, pr, results, roc = validate_model_for_epoch(best_validation_score, epoch, model,
                                                                           model_name, pr_history, preprocessing_method,
                                                                           roc_history, train_dataset, val_dataset,
                                                                           valid_batch_gen)
        train_acc_metric.reset_states()

        # Early-Stopping
        if early_stopping_epochs and len(roc_history) > early_stopping_epochs:
            stop_criteria = True
            for i in range(1, early_stopping_epochs + 1):
                stop_criteria = stop_criteria and (roc_history[-i] + pr_history[-i]) < best_validation_score
            if stop_criteria:
                break

        # log metrics using wandb.log
        wandb.log({'epochs': epoch,
                   'acc': float(train_acc),
                   'val_roc_all': float(roc),
                   'val_pr_all': float(pr),
                   'val_roc_genre': results.get_cat_results('genre')[0],
                   'val_pr_genre': results.get_cat_results('genre')[1],
                   'val_roc_instrument': results.get_cat_results('instrument')[0],
                   'val_pr_instrument': results.get_cat_results('instrument')[1],
                   'val_roc_mood': results.get_cat_results('mood')[0],
                   'val_pr_mood': results.get_cat_results('mood')[1]})
    avg_time = 0
    for val in epoch_times:
        avg_time += val
    if epochs_count > 0:
        wandb.log({'avg_epoch_time_minutes': avg_time / epochs_count / 60})
    return True


def check_for_bad_learning_rate_criteria(avg_loss_history, early_lr_stopping, train_batch_gen, train_loss):
    return early_lr_stopping and len(train_loss) > (len(train_batch_gen) / 10) and \
           avg_loss_history.__len__() > 0 and avg_loss_history[-1] > early_lr_stopping


def log_train_loss(avg_loss_history, log_step, train_loss):
    if len(train_loss) > 0 and len(train_loss) % log_step == 0:
        avg_loss = np.mean(train_loss[(-log_step - 1):-1])
        avg_loss_history.append(avg_loss)
        # plot_loss_history(avg_loss_history, log_step)
        wandb.log({'train_loss': float(avg_loss)})


def train_step_from_batch(batch, loss_fn, model, optimizer, train_acc_metric, train_loss):
    images, labels = batch.data, batch.label
    labels = tf.convert_to_tensor(labels, dtype=tf.int32)
    images = tf.convert_to_tensor(images, dtype=tf.float16)
    loss_value = train_step_from_input_and_labels(images, labels,
                                                  model, optimizer,
                                                  loss_fn, train_acc_metric)
    train_loss.append(float(loss_value))


def validate_model_for_epoch(best_validation_score, epoch, model, model_name, pr_history, preprocessing_method,
                             roc_history, train_dataset, val_dataset, valid_batch_gen):
    print(f'Start validating for epoch {epoch}...')
    results = EvaluationResult(model, valid_batch_gen, val_dataset, preprocessing_method, model)
    roc, pr = results.get_all_categories_results()
    roc_history.append(roc)
    pr_history.append(pr)
    best_validation_score = check_and_save_model(best_validation_score, model, model_name, pr, preprocessing_method,
                                                 roc, train_dataset.dataset_name)
    print(results)
    return best_validation_score, pr, results, roc


def train_step_from_input_and_labels(x, y, model, optimizer, loss_fn, train_acc_metric):
    with tf.GradientTape() as tape:
        logits = model(x, training=True)
        loss = loss_fn(y, logits)
        scaled_loss = optimizer.get_scaled_loss(loss)
    scaled_gradients = tape.gradient(scaled_loss, model.trainable_weights)
    grads = optimizer.get_unscaled_gradients(scaled_gradients)
    optimizer.apply_gradients(zip(grads, model.trainable_weights))

    train_acc_metric.update_state(y, logits)

    return loss


def plot_loss_history(history: list, loss_step_size: int) -> None:
    plt.close()
    plt.plot([idx * loss_step_size for idx, val in enumerate(history)], history)
    plt.legend(('Loss', 'True'))
    plt.xlabel('Iteration')
    plt.ylabel('Loss')
    plt.show()


def get_optimizer_by_config(learning_rate: float, weight_decay: float, piecewise_constant_decay: list,
                            piecewise_constant_decay_values: list, optimizer: str,
                            momentum: float, nesterov: bool) -> tf.optimizers.Optimizer:
    if piecewise_constant_decay:
        lr_schedule = tf.optimizers.schedules.PiecewiseConstantDecay(
            piecewise_constant_decay, [elem * learning_rate for elem in piecewise_constant_decay_values])
        learning_rate = lr_schedule
        if weight_decay:
            wd_schedule = tf.optimizers.schedules.PiecewiseConstantDecay(
                piecewise_constant_decay, [elem * weight_decay for elem in piecewise_constant_decay_values])
            weight_decay = wd_schedule
    if weight_decay:
        if optimizer.lower() == 'adam':
            optimizer_out = tfa.optimizers.AdamW(weight_decay, learning_rate)
        elif optimizer.lower() == 'sgd':
            optimizer_out = tfa.optimizers.SGDW(weight_decay, learning_rate, momentum, nesterov)
        else:
            raise NotImplementedError(f'Optimizer {optimizer} not implemented')
    else:
        if optimizer.lower() == 'adam':
            optimizer_out = tf.keras.optimizers.Adam(learning_rate)
        elif optimizer.lower() == 'sgd':
            optimizer_out = tf.keras.optimizers.SGD(learning_rate, momentum, nesterov)
        elif optimizer.lower() == 'rms_prop':
            optimizer_out = tf.keras.optimizers.RMSprop(learning_rate)
        else:
            raise NotImplementedError(f'Optimizer {optimizer} not implemented')
    optimizer_out = tf.keras.mixed_precision.experimental.LossScaleOptimizer(optimizer_out, 'dynamic')
    return optimizer_out


def scale_scheduler_decay(piecewise_constant_decay: list, training_set: Dataset, batch_size: int):
    return [elem * math.floor(len(training_set) / batch_size) for elem in piecewise_constant_decay]


def run_multi_configs_with_wandb():
    wandb.login(key=env.wandb_key)
    config_gpu(env.VISIBLE_DEVICES)
    configs = [vgg16_configs, senet_configs, resnet_configs, musicnn_configs, dil_cnn_configs]
    for config in configs:
        for confs in config:
            try:
                if confs.seed == None:
                    raise ValueError('Training Seed must be provided!')
                run = wandb.init(config=confs, reinit=True, tags=['eval', confs.get('dataset')])
                execute_run_from_configured_wandb_instance(run)
            except Exception as e:
                print('run-config failed')
                print(e)


def execute_run_from_configured_wandb_instance(run):
    config = wandb.config
    seed = config.seed
    random.seed(seed)
    wandb.log({'seed': seed})

    test_set, training_set, validation_set = get_datasets_from_config(config, use_preprocessed_input=True)
    test_batch_gen = BatchGenerator(test_set, config.test_batch_size, False, config.parallel_input_processing)
    cnn_model = get_model(config.model, training_set.num_tags(), training_set.shape, config.dropout,
                          config.l2_regularizer, config.batch_norm)

    if config.load_old_model:
        cnn_model.load_weights(
            os.path.join('trained_models', config.dataset, config.model, config.preprocessing_method))
    scheduler_decay = init_scheduler_decay(config, training_set)
    optimizer = get_optimizer_by_config(config.learning_rate, config.weight_decay, scheduler_decay,
                                        config.piecewise_constant_decay_values,
                                        config.optimizer, config.momentum, config.nesterov)
    second_optimizer = init_second_optimizer(config, training_set)
    train_acc_metric = tf.keras.metrics.BinaryAccuracy()
    if config.log_step is None:
        log_step = int(len(training_set) / config.batch_size / 25)
    else:
        log_step = config.log_step
    trained = train_model(training_set,
                          validation_set,
                          config.batch_size,
                          config.preprocessing_method,
                          cnn_model,
                          config.model,
                          optimizer,
                          config.test_batch_size,
                          epochs=config.epochs,
                          loss_fn=tf.keras.losses.BinaryCrossentropy(),
                          log_step=log_step,
                          early_stopping_epochs=config.early_stopping_epochs,
                          train_acc_metric=train_acc_metric,
                          early_lr_stopping=config.early_lr_stopping,
                          second_optimizer=second_optimizer,
                          second_optimizer_epochs=config.use_sgd_epoch,
                          parallel_input_processing=config.parallel_input_processing)
    if trained:
        test_model(cnn_model, config, test_batch_gen, test_set)
    else:
        wandb.log({'err': 'Bad LR'})
    run.finish()


def init_second_optimizer(config, training_set):
    if config.use_sgd_epoch is not None:
        sgd_scheduler_decay = scale_scheduler_decay(config.sgd_piecewise_constant_decay, training_set,
                                                    config.batch_size)
        second_optimizer = get_optimizer_by_config(config.sgd_scheduler_lr, config.weight_decay,
                                                   sgd_scheduler_decay,
                                                   config.sgd_piecewise_constant_decay_values,
                                                   'sgd', config.momentum, config.nesterov)
    else:
        second_optimizer = None
    return second_optimizer


def init_scheduler_decay(config, training_set):
    if config.piecewise_constant_decay is not None:
        scheduler_decay = scale_scheduler_decay(config.piecewise_constant_decay, training_set,
                                                config.batch_size)
    else:
        scheduler_decay = None
    return scheduler_decay


def run_with_wandb(config):
    wandb.login(key=env.wandb_key)
    run = wandb.init(config=config, )
    config_gpu(env.VISIBLE_DEVICES)
    execute_run_from_configured_wandb_instance(run)


def get_datasets_from_config(config, use_preprocessed_input):
    if config.dataset == 'magna_tag_a_tune':
        training_set = MagnaTagATune(use_preprocessed_input, PreprocessingMethod[config.preprocessing_method],
                                     Subset.TRAINING, config.set_split, config.chunk_lvl, config.nr_samples)
        validation_set = MagnaTagATune(use_preprocessed_input, PreprocessingMethod[config.preprocessing_method],
                                       Subset.VALIDATION, config.set_split, config.chunk_lvl, config.nr_samples)
        test_set = MagnaTagATune(use_preprocessed_input, PreprocessingMethod[config.preprocessing_method], Subset.TEST,
                                 config.set_split, config.chunk_lvl, config.nr_samples)
    else:
        training_set = MtgJamendo(use_preprocessed_input, PreprocessingMethod[config.preprocessing_method],
                                  Subset.TRAINING, config.chunk_lvl, config.nr_samples, config.set_split)
        validation_set = MtgJamendo(use_preprocessed_input, PreprocessingMethod[config.preprocessing_method],
                                    Subset.VALIDATION, config.chunk_lvl, config.nr_samples, config.set_split)
        test_set = MtgJamendo(use_preprocessed_input, PreprocessingMethod[config.preprocessing_method], Subset.TEST,
                              config.chunk_lvl, config.nr_samples, config.set_split)
    return test_set, training_set, validation_set


def test_model(cnn_model, config, test_batch_gen, test_set):
    print('Start testing model on test-dataset...')
    cnn_model.load_weights(os.path.join('trained_models', config.dataset, config.model, config.preprocessing_method))
    results = EvaluationResult(cnn_model, test_batch_gen, test_set, config.preprocessing_method, config.model)
    print(results)
    results.save_results()
    roc, pr = results.get_all_categories_results()
    wandb.log({'test_roc': float(roc),
               'test_pr': float(pr),
               'test_roc_genre': results.get_cat_results('genre')[0],
               'test_pr_genre': results.get_cat_results('genre')[1],
               'test_roc_instrument': results.get_cat_results('instrument')[0],
               'test_pr_instrument': results.get_cat_results('instrument')[1],
               'test_roc_mood': results.get_cat_results('mood')[0],
               'test_pr_mood': results.get_cat_results('mood')[1]})


def config_gpu(device):
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    if device is not None:
        os.environ["CUDA_VISIBLE_DEVICES"] = device
    if tf.config.list_physical_devices('GPU'):
        physical_devices = tf.config.list_physical_devices('GPU')
        tf.config.experimental.set_memory_growth(physical_devices[0], enable=True)
        tf.config.experimental.set_virtual_device_configuration(physical_devices[0], [
            tf.config.experimental.VirtualDeviceConfiguration(memory_limit=env.MEMORY_LIMIT)])
    tf.keras.mixed_precision.experimental.set_policy('mixed_float16')


def init_all_from_mtgj():
    MtgJamendo(True, PreprocessingMethod[PreprocessingMethod.RAW_WAVEFORM.name], Subset.TRAINING, False, None, (60, 20, 20))
    MtgJamendo(True, PreprocessingMethod[PreprocessingMethod.STFT.name], Subset.TRAINING, False, None, (60, 20, 20))
    MtgJamendo(True, PreprocessingMethod[PreprocessingMethod.MEL_SPECTROGRAM.name], Subset.TRAINING, False, None, (60, 20, 20))
    MtgJamendo(True, PreprocessingMethod[PreprocessingMethod.MFCC.name], Subset.TRAINING, False, None, (60, 20, 20))
    MtgJamendo(True, PreprocessingMethod[PreprocessingMethod.CQT_TRANSFORMATION.name], Subset.TRAINING, False, None, (60, 20, 20))


def create_config():
    return {
        "dataset": 'mtg_jamendo',
        "optimizer": 'adam',
        "learning_rate": 0.002,
        "weight_decay": None,
        "piecewise_constant_decay": [10, 20, 30],
        "piecewise_constant_decay_values": [1e-0, 0.2, 0.05, 0.02],
        "use_sgd_epoch": None,
        "sgd_scheduler_lr": 2e-5,
        "sgd_scheduler_wd": None,
        "sgd_piecewise_constant_decay": [2, 4],
        "sgd_piecewise_constant_decay_values": [1e-0, 0.25, 5e-2],
        "momentum": 0.9,
        "nesterov": False,
        "epochs": 40,
        "batch_size": 16,
        "test_batch_size": 32,
        "log_step": None,
        "nr_samples": 3000,
        "preprocessing_method": PreprocessingMethod.MEL_SPECTROGRAM.name,
        "model": 'senet',
        "set_split": (60, 20, 20),
        "chunk_lvl": True,
        "load_old_model": False,
        "early_stopping_epochs": 6,
        "early_lr_stopping": None,
        "dropout": 0.0,
        "l2_regularizer": 0.01,
        "batch_norm": True,
        "parallel_input_processing": None,
        "seed": 88134245
    }


if __name__ == '__main__':
    config = create_config()
    run_with_wandb(config)
    # run_multi_configs_with_wandb()
    # init_all_from_mtgj()
