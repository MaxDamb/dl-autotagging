import datetime
import math
import random
import sys

import pandas as pd
import os
from joblib import Parallel, delayed
import librosa
import librosa.display
import numpy as np
from tqdm import tqdm
import abc
from enum import Enum
import zarr

from env import NR_CPUS


class Subset(Enum):
    '''
    Dataset subsets.
    '''

    TRAINING = 0
    VALIDATION = 1
    TEST = 2


class PreprocessingMethod(Enum):
    RAW_WAVEFORM = 'raw_waveform'
    STFT = 'stft'
    STFT_LOG = 'stft_log'
    MEL_SPECTROGRAM = 'mel_spectrogram'
    MFCC = 'mfcc'
    CQT_TRANSFORMATION = 'cqt'


class Dataset(metaclass=abc.ABCMeta):
    """Define a dataset, that provides methods for selecting preprocessing methods and fetching data from the dataset

    New Datasets shall extend this class and implement all methods accordingly
    """

    @abc.abstractmethod
    def __init__(self, use_preprocessed_input: bool, preprocessing_method: PreprocessingMethod, subset: Subset,
                 split: (int, int, int), chunk_lvl: bool, nr_samples: int):
        """ initialize the dataset with the given configuration

        :param use_preprocessed_input: preprocess the given transformation during initialization if true and then only
        load from hard-drive during training, if false then preprocess during training
        :param preprocessing_method: the respective preprocessing-method to use for this dataset-instance
        must define dataset name in each child-class
        :param split: the dataset split, define size of training, validation and test dataset-relative to each other,
        :param nr_samples: if set, then use only this amount of datasamples for all data-splits
        """
        self.dataset_name = 'dataset'
        self.subset = subset
        self.preprocessing_method = preprocessing_method
        self.chunk_lvl = chunk_lvl
        # split each file into this nr of sub-chunks
        if chunk_lvl:
            self.chunk_length = 3
        else:
            self.chunk_length = 30
        self.last_sample = None
        self.last_sample_path = None
        self.annotations = None
        # If true, then only receive a random single chunk at chunk lvl, dataset length is then only total song_nr
        self.random_single_chunk = False

    @abc.abstractmethod
    def __len__(self) -> int:
        """
        Returns the number of samples in the dataset.
        """

        pass

    @abc.abstractmethod
    def __getitem__(self, idx: slice) -> (np.array, np.array):
        """
        Returns the idx-th elements in the dataset with sample and label
        Raises IndexError if the index is out of bounds.
        """

        pass

    @abc.abstractmethod
    def get_input_shape(self) -> (int, int):
        """Returns the shape of the the data-tuples (for the respective input representation)"""

        pass

    @abc.abstractmethod
    def num_tags(self) -> int:
        """Returns the number of different tags"""

        pass

    @abc.abstractmethod
    def get_song_chunks(self, i) -> (np.array, np.array):
        """
        :param i: song index
        :return: all chunks of this song
        """
        pass

    def get_song_nr(self) -> int:
        if self.annotations is not None:
            return len(self.annotations)
        else:
            return -1

    def preprocess_song(self, song_path, sr=16000, duration=29.15):
        x, sr = librosa.load(os.path.join('datasets', self.dataset_name, 'audio_files', song_path), duration=duration)
        preprocessing_method_switch = {
            PreprocessingMethod.STFT: lambda input: np.abs(librosa.stft(input, hop_length=512, )),
            PreprocessingMethod.RAW_WAVEFORM: lambda input: input,
            PreprocessingMethod.STFT_LOG: lambda input: get_decibel_stft(input),
            PreprocessingMethod.MEL_SPECTROGRAM: lambda input: librosa.power_to_db(
                librosa.feature.melspectrogram(input, sr=sr, n_mels=96, n_fft=1024, hop_length=512), ref=np.max),
            PreprocessingMethod.MFCC: lambda input: librosa.feature.mfcc(input, sr=sr),
            PreprocessingMethod.CQT_TRANSFORMATION: lambda input: librosa.amplitude_to_db(
                np.abs(librosa.cqt(input, sr=sr, n_bins=96)), ref=np.max)
        }
        processed = preprocessing_method_switch.get(self.preprocessing_method)(x)
        if len(processed.shape) == 2:
            processed = processed.reshape(processed.shape[0], processed.shape[1], 1)
        if len(processed.shape) == 1:
            processed = processed.reshape(processed.shape[0], 1)
        return processed

    def check_and_save_input_representation(self):
        if not os.path.exists(os.path.join('datasets', self.dataset_name, self.preprocessing_method.name)):
            self.__save_all_inputs_parallel()

    def annotations_to_samples(self, idx: int) -> np.array:
        path = os.path.join('datasets', self.dataset_name, self.preprocessing_method.name,
                            self.annotations['mp3_path'].values[idx] + '.zarr')
        return zarr.load(path)

    def get_datasplit_from_annotations(self, annotations: pd.DataFrame, subset: Subset,
                                       split: (int, int, int)) -> pd.DataFrame:
        split_total = split[0] + split[1] + split[2]
        start_index = 0
        end_index = split[0]
        if subset.value > 0:
            start_index += split[0]
            end_index += split[1]
        if subset.value > 1:
            start_index += split[1]
            end_index += split[2]
        start_index = math.floor(len(annotations) * start_index / split_total)
        end_index = math.floor(len(annotations) * end_index / split_total)
        return annotations[start_index:end_index].reset_index(drop=True)

    def __save_all_inputs_parallel(self):
        num_cores = NR_CPUS
        Parallel(n_jobs=num_cores)(
            delayed(lambda input: self.__save_input_representation_of_song(input))(name) for name in
            tqdm(self.annotations['mp3_path'].to_numpy()))

    def __save_input_representation_of_song(self, song_path):
        if os.path.exists(os.path.join('datasets', self.dataset_name, 'audio_files', song_path) and (
                not os.path.exists(
                    os.path.join('datasets', self.dataset_name, self.preprocessing_method.name, song_path + '.zarr')))):
            processed = self.preprocess_song(song_path)
            path = os.path.join('datasets', self.dataset_name, self.preprocessing_method.name, song_path)
            create_dirs_without_file(path)
            zarr.save(f'{path}.zarr', processed)


class MtgJamendo(Dataset):

    def __init__(self, use_preprocessed_input: bool, preprocessing_method: PreprocessingMethod, subset: Subset,
                 chunk_lvl: bool, nr_samples: int, split: (int, int, int) = None):
        super().__init__(use_preprocessed_input, preprocessing_method, subset, split, chunk_lvl, nr_samples)
        self.dataset_name = 'mtg_jamendo'
        self._sample_length = 30
        self.random_single_chunk = True
        use_predefined_split = split is None
        self.annotations = self.__reduce_annotations(subset, use_predefined_split)
        if nr_samples:
            self.annotations = self.annotations[0: nr_samples]
        self.use_preprocessed_input = use_preprocessed_input
        if use_preprocessed_input:
            super().check_and_save_input_representation()
        if split is not None:
            self.annotations = super().get_datasplit_from_annotations(self.annotations, subset, split)
        self.annotations_np = self.annotations.to_numpy()
        self._set_shape()
        self._set_dataset_len()
        self._set_song_chunk_indices()

    def __len__(self) -> int:
        return self.dataset_len

    def __getitem__(self, idx: int) -> (np.array, np.array):
        if self.use_preprocessed_input:
            sample = super().annotations_to_samples(idx)
        else:
            sample = super().preprocess_song(self.annotations.loc[idx]['mp3_path'])

        chunk_length = self.shape[0] if self.preprocessing_method == PreprocessingMethod.RAW_WAVEFORM else self.shape[1]
        full_song_length = sample.shape[0] if self.preprocessing_method == PreprocessingMethod.RAW_WAVEFORM else \
            sample.shape[1]
        start_idx = random.randint(0, full_song_length - chunk_length)
        end_idx = start_idx + chunk_length
        if self.preprocessing_method == PreprocessingMethod.RAW_WAVEFORM:
            return sample[start_idx:end_idx, :], self._annotations_to_labels(idx)
        else:
            return sample[:, start_idx:end_idx, :], self._annotations_to_labels(idx)

    def get_song_chunks(self, song_idx) -> (np.array, np.array):
        if self.use_preprocessed_input:
            sample = super().annotations_to_samples(song_idx)
        else:
            sample = super().preprocess_song(self.annotations.loc[song_idx]['mp3_path'])
        data = []
        labels = []
        for chunk_idx in range(0, self.__chunk_lengths[song_idx]):
            end_idx, start_idx = self._get_index_range_for_chunk(chunk_idx, sample, song_idx)
            if self.preprocessing_method == PreprocessingMethod.RAW_WAVEFORM:
                data.append(sample[start_idx:end_idx, :])
                labels.append(self._annotations_to_labels(song_idx))
            else:
                data.append(sample[:, start_idx:end_idx, :])
                labels.append(self._annotations_to_labels(song_idx))
        return np.array(data), np.array(labels)

    def _get_index_range_for_chunk(self, chunk_idx, sample, song_idx):
        song_length = self.shape[0] if self.preprocessing_method == PreprocessingMethod.RAW_WAVEFORM else self.shape[1]
        total_chunks = self.__chunk_lengths[song_idx]
        if chunk_idx < total_chunks - 1:
            start_idx = song_length * chunk_idx
            end_idx = song_length * (chunk_idx + 1)
        else:
            end_idx = sample.shape[0] if self.preprocessing_method == PreprocessingMethod.RAW_WAVEFORM else \
                sample.shape[1]
            start_idx = end_idx - song_length
        return end_idx, start_idx

    def get_input_shape(self) -> (int, int):
        return self.shape

    def num_tags(self) -> int:
        return self.annotations.shape[1] - 2

    def _annotations_to_labels(self, idx: int) -> np.array:
        return self.annotations_np[idx:idx + 1, 0: -2][0]

    def _set_dataset_len(self):
        self.dataset_len = len(self.annotations)

    def __reduce_annotations(self, subset: Subset, use_predefined_split=False) -> pd.DataFrame:
        if not use_predefined_split:
            subset = None
        file_name = self._get_annotation_file_name_from_subset(subset)
        path = os.path.join('datasets', self.dataset_name, 'audio_labels', file_name)
        if os.path.exists(path):
            return pd.read_csv(path)
        else:
            annotations_df = self._load_annotations_file(subset)
            tags = pd.read_csv(
                os.path.join('datasets', self.dataset_name, 'audio_labels', 'label_categories.csv'))
            columns = tags['label']
            columns = columns.append(pd.Series('mp3_path'))
            reduced_annotations_df = pd.DataFrame(columns=columns)
            reduced_annotations_df = self._append_reduced_tagset_to_annotations_df(annotations_df,
                                                                                   reduced_annotations_df)
            reduced_annotations_df.drop(index=0, axis=1)
            reduced_annotations_df.to_csv(os.path.join('datasets', self.dataset_name, 'audio_labels', file_name))
            return reduced_annotations_df

    def _get_annotation_file_name_from_subset(self, subset):
        file_name = {
            None: 'annotations_merged.csv',
            Subset.TRAINING: 'annotations_train_merged.csv',
            Subset.TEST: 'annotations_test_merged.csv',
            Subset.VALIDATION: 'annotations_validation_merged.csv',
        }
        file_name = file_name.get(subset)
        return file_name

    def _load_annotations_file(self, subset):
        tags_file = {
            None: 'labeled_tracks.tsv',
            Subset.TRAINING: 'autotagging_top50tags-train.tsv',
            Subset.TEST: 'autotagging_top50tags-test.tsv',
            Subset.VALIDATION: 'autotagging_top50tags-validation.tsv',
        }
        tags_file = tags_file.get(subset)
        annotations = pd.read_csv(
            os.path.join('datasets', self.dataset_name, 'audio_labels', tags_file),
            sep='\t', header=0)
        return annotations

    def _append_reduced_tagset_to_annotations_df(self, annotations_df, reduced_annotations_df):
        for line in annotations_df.values:
            new_line = pd.Series(0, index=reduced_annotations_df.columns)
            new_line['mp3_path'] = line[3]
            for tag in line[5:]:
                if not isinstance(tag, str) and math.isnan(tag):
                    break
                else:
                    tag = tag.split('---')[1]
                    new_line[tag] = 1
                new_line['duration'] = line[4]
            reduced_annotations_df = reduced_annotations_df.append(new_line, ignore_index=True)
        return reduced_annotations_df

    def _set_shape(self):
        self.shape = super().preprocess_song(self.annotations.at[0, 'mp3_path']).shape
        sample_len = self._sample_length
        if self.preprocessing_method == PreprocessingMethod.RAW_WAVEFORM:
            self.shape = int((self.shape[0] / sample_len) * self.chunk_length), self.shape[1]
        else:
            self.shape = self.shape[0], int((self.shape[1] / sample_len) * self.chunk_length), 1

    def _set_song_chunk_indices(self):
        self.__chunk_idxs = {}
        self.__song_idxs = {}
        self.__chunk_lengths = {}
        i = 0
        for song_idx, val in enumerate(self.annotations['duration'].values):
            song_chunk_nr = - int(self._sample_length // - self.chunk_length)
            self.__chunk_lengths[song_idx] = song_chunk_nr
            for song_chunk_idx in range(i, i + song_chunk_nr):
                self.__song_idxs[song_chunk_idx] = song_idx
                self.__chunk_idxs[song_chunk_idx] = song_chunk_idx - i
            i += song_chunk_nr

    def __chunk_idx_to_song_and_subchunk(self, idx: int) -> (int, int):
        song_idx = self.__song_idxs[idx]
        return song_idx, self.__chunk_idxs[idx], self.__chunk_lengths[song_idx]


class MagnaTagATune(Dataset):

    def __init__(self, use_preprocessed_input: bool, preprocessing_method: PreprocessingMethod, subset: Subset,
                 split: (int, int, int), chunk_lvl: bool, nr_samples: int = None):
        super().__init__(use_preprocessed_input, preprocessing_method, subset, split, chunk_lvl, nr_samples)
        self.dataset_name = 'magna_tag_a_tune'
        self._sample_length = 30
        self.annotations = self._preprocess_and_merge_annotations()
        if nr_samples:
            self.annotations = self.annotations[0: nr_samples]
        self.use_preprocessed_input = use_preprocessed_input
        self._set_shape()
        if use_preprocessed_input:
            super().check_and_save_input_representation()
        self.annotations = super().get_datasplit_from_annotations(self.annotations, subset, split)
        self.annotations_np = self.annotations.to_numpy()

    def __len__(self) -> int:
        return len(self.annotations) * int(self._sample_length / self.chunk_length)

    def __getitem__(self, idx: int) -> (np.array, np.array):
        song_idx, chunk_idx = self._chunk_idx_to_song_and_subchunk(idx)
        sample = self._get_sample_from_song_idx(song_idx)

        if self.chunk_lvl:
            if self.preprocessing_method == PreprocessingMethod.RAW_WAVEFORM:
                return self._get_waveform_item(chunk_idx, sample, song_idx)
            else:
                return self._get_spectrogram_item(chunk_idx, sample, song_idx)
        else:
            return sample, self._annotations_to_labels(song_idx)

    def _get_sample_from_song_idx(self, song_idx):
        if self.use_preprocessed_input:
            sample = super().annotations_to_samples(song_idx)
        else:
            sample = super().preprocess_song(self.annotations.loc[song_idx]['mp3_path'])
        return sample

    def _get_waveform_item(self, chunk_idx, sample, song_idx):
        sample = np.pad(sample, ((0, self.shape[0]), (0, 0)))
        start_idx = int(chunk_idx * self.shape[0] + int(random.uniform(0, 0.6) * self.shape[0]))
        end_idx = int(start_idx + self.shape[0])
        return sample[start_idx:end_idx, :], self._annotations_to_labels(song_idx)

    def _get_spectrogram_item(self, chunk_idx, sample, song_idx):
        sample = np.pad(sample, ((0, 0), (0, self.shape[1]), (0, 0)))
        start_idx = int(chunk_idx * self.shape[1] + int(random.uniform(0, 0.6) * self.shape[1]))
        end_idx = int(start_idx + self.shape[1])
        return sample[:, start_idx:end_idx, :], self._annotations_to_labels(song_idx)

    def get_song_chunks(self, song_idx) -> (np.array, np.array):
        total_chunks = int(30 / self.chunk_length)
        sample = self._get_sample_from_song_idx(song_idx)

        data = []
        labels = []
        for chunk_idx in range(0, total_chunks):
            if self.preprocessing_method == PreprocessingMethod.RAW_WAVEFORM:
                data.append(self._get_waveform_chunk_data(chunk_idx, sample))
                labels.append(self._annotations_to_labels(song_idx))
            else:
                data.append(self._get_spectrogram_chunk_data(chunk_idx, sample))
                labels.append(self._annotations_to_labels(song_idx))
        return np.array(data), np.array(labels)

    def _get_waveform_chunk_data(self, chunk_idx, sample):
        sample = np.pad(sample, ((0, self.shape[0]), (0, 0)))
        start_idx = chunk_idx * self.shape[0] + int(random.uniform(0, 0.6) * self.shape[0])
        end_idx = start_idx + self.shape[0]
        return sample[start_idx:end_idx, :]

    def _get_spectrogram_chunk_data(self, chunk_idx, sample):
        sample = np.pad(sample, ((0, 0), (0, self.shape[1]), (0, 0)))
        start_idx = chunk_idx * self.shape[1] + int(random.uniform(0, 0.6) * self.shape[1])
        end_idx = start_idx + self.shape[1]
        return sample[:, start_idx:end_idx, :]

    def get_input_shape(self) -> (int, int):
        return self.shape

    def num_tags(self) -> int:
        return self.annotations.shape[1] - 1

    def _preprocess_and_merge_annotations(self) -> pd.DataFrame:
        path = os.path.join('datasets', self.dataset_name, 'audio_labels', 'annotations_merged.csv')
        if os.path.exists(path):
            return pd.read_csv(path)
        else:
            synonyms = self._get_mtat_synonyms()
            old_annotations = pd.read_csv(
                os.path.join('datasets', self.dataset_name, 'audio_labels', 'annotations_final.csv'), sep='\t')
            for synonym_list in synonyms:
                old_annotations[synonym_list[0]] = old_annotations[synonym_list].max(axis=1)
                old_annotations = old_annotations.drop(synonym_list[1:], axis=1)
            old_annotations = old_annotations.drop('clip_id', axis=1)
            top_label_tuples = []
            for label in old_annotations.columns:
                top_label_tuples = self._append_top_50_tag_labels(label, old_annotations, top_label_tuples)
            for label in old_annotations.columns:
                if not label in [val[0] for val in top_label_tuples] and not label == 'mp3_path':
                    old_annotations = old_annotations.drop(label, axis=1)
            old_annotations.drop(index=0, axis=1)
            old_annotations.to_csv(
                os.path.join('datasets', self.dataset_name, 'audio_labels', 'annotations_merged.csv'))
            return old_annotations

    def _append_top_50_tag_labels(self, label, old_annotations, top_label_tuples):
        smallest_tuple = ('', sys.maxsize)
        for tuple in top_label_tuples:
            if tuple[1] < smallest_tuple[1]:
                smallest_tuple = tuple
        label_occurences = self._get_nr_of_occurences(label, old_annotations)
        if len(top_label_tuples) < 50:
            top_label_tuples.append((label, label_occurences))
        elif label_occurences > smallest_tuple[1]:
            top_label_tuples = [tuple for tuple in top_label_tuples if tuple != smallest_tuple]
            top_label_tuples.append((label, label_occurences))
        return top_label_tuples

    def _get_mtat_synonyms(self):
        synonyms = [['beat', 'beats'],
                    ['chant', 'chanting'],
                    ['choir', 'choral'],
                    ['classical', 'clasical', 'classic'],
                    ['drum', 'drums'],
                    ['electro', 'electronic', 'electronica', 'electric'],
                    ['fast', 'fast beat', 'quick'],
                    ['female', 'female singer', 'female singing', 'female vocals', 'female voice', 'woman',
                     'woman singing', 'women'],
                    ['flute', 'flutes'],
                    ['guitar', 'guitars'],
                    ['hard', 'hard rock'],
                    ['harpsichord', 'harpsicord'],
                    ['heavy', 'heavy metal', 'metal'],
                    ['horn', 'horns'],
                    ['india', 'indian'],
                    ['jazz', 'jazzy'],
                    ['male', 'male singer', 'male vocal', 'male vocals', 'male voice', 'man', 'man singing', 'men'],
                    ['no beat', 'no drums'],
                    ['no singer', 'no singing', 'no vocal', 'no vocals', 'no voice', 'no voices', 'instrumental'],
                    ['opera', 'operatic'],
                    ['orchestra', 'orchestral'],
                    ['quiet', 'silence'],
                    ['singer', 'singing'],
                    ['space', 'spacey'],
                    ['string', 'strings'],
                    ['synth', 'synthesizer'],
                    ['violin', 'violins'],
                    ['vocal', 'vocals', 'voice', 'voices'],
                    ['strange', 'weird']]
        return synonyms

    def _set_shape(self):
        chunk_nr = (30 / self.chunk_length)
        self.shape = super().preprocess_song(self.annotations.at[0, 'mp3_path']).shape
        if self.chunk_lvl:
            if self.preprocessing_method == PreprocessingMethod.RAW_WAVEFORM:
                self.shape = math.floor(self.shape[0] / chunk_nr), self.shape[1]
            else:
                self.shape = self.shape[0], math.floor(self.shape[1] / chunk_nr), 1

    def _chunk_idx_to_song_and_subchunk(self, idx: int) -> (int, int):
        chunk_nr = (30 / self.chunk_length)
        if self.chunk_lvl:
            return math.floor(idx / chunk_nr), idx % chunk_nr
        else:
            return idx, 0

    def _get_nr_of_occurences(self, column_name: str, annotations) -> int:
        return annotations[column_name].value_counts()[1]

    def _annotations_to_labels(self, idx: int) -> np.array:
        return self.annotations_np[idx:idx + 1, 0: -1][0]


def get_decibel_stft(x: np.array):
    stft = np.abs(librosa.stft(x, hop_length=512))
    return librosa.amplitude_to_db(stft, ref=np.max)


def create_dirs_without_file(path: str):
    # remove the filename from the path
    path = '/'.join(path.split("/")[0:-1])
    if (not os.path.exists(path)):
        os.makedirs(path)
