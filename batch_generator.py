import math
import multiprocessing
import queue
import random

import numpy as np
import typing

from preprocessing import Dataset, Subset


class Batch:
    def __init__(self, data: np.array, label: np.array, idx: np.array):
        self.data = data
        self.label = label
        self.idx = idx


class BatchGenerator:
    """
    Batch generator.
    Returned batches have the following properties:
      data: numpy array holding batch data of shape (l, DATASET_SAMPLE_SHAPE).
      label: numpy array holding batch labels of shape (l, DATASET_LABEL_SHAPE).
      idx: numpy array with shape (l,) encoding the indices of each sample in the original dataset.
    """

    def __init__(self, dataset: Dataset, batch_size: int, shuffle: bool, parallel_input_processing=None):
        '''
        Dataset is the dataset to iterate.
        batch_size is the number of samples per batch. the number in the last batch might be smaller than that.
        shuffle if true then the order of dataset gets shuffled, if false it stays as initialized.
        '''
        self.dataset = dataset
        self.batch_size = batch_size
        self.shuffle = shuffle
        if batch_size > len(dataset):
            raise ValueError('Batch size must be smaller or equal to length of dataset')
        self.indices = list(range(0, len(dataset)))

        self.output_queue = multiprocessing.Queue()
        self.index_queues = []
        self.num_workers = parallel_input_processing
        self.workers = []
        # self.worker_cycle = itertools.cycle(range(self.num_workers))
        self.cache = {}
        self.prefetch_index = 0
        self.index = 0
        self.index_queue = multiprocessing.Queue()

        if self.dataset.subset == Subset.TRAINING and parallel_input_processing is not None:
            self._init_workers_for_parallel_input_processing()

    def _init_workers_for_parallel_input_processing(self):
        for _ in range(self.num_workers):
            # index_queue = multiprocessing.Queue()
            worker = multiprocessing.Process(
                target=self._prefetch_worker_fn, args=(self.index_queue, self.output_queue)
            )
            worker.daemon = True
            worker.start()
            self.workers.append(worker)
            # self.index_queues.append(index_queue)
        self.__reset()

    def __len__(self) -> int:
        """
        Returns the number of batches generated per iteration.
        """
        return math.floor(len(self.dataset) / self.batch_size)

    def __iter__(self) -> typing.Iterable[Batch]:
        """
        Iterate over the dataset, returning the data as batches
        iterate exactly one epoch
        after each full iteration, data is shuffled
        """
        if self.dataset.chunk_lvl and not self.dataset.subset == Subset.TRAINING:
            yield from self._yield_all_subchunks_for_each_song_per_batch()
        else:
            yield from self._yield_multiple_songs_batches_from_dataset()

        self.__reset()

    def _yield_multiple_songs_batches_from_dataset(self):
        for i in range(0, len(self.dataset), self.batch_size):
            all_data = []
            labels = []
            idx = []
            for chunk_idx in range(i, i + self.batch_size):
                if chunk_idx < len(self.indices):
                    data, label, index = self._get_next_batch_item()
                    all_data.append(data)
                    labels.append(label)
                    idx.append(index)
            batch = Batch(np.array(all_data), np.array(labels), np.array(idx))
            yield batch

    def _yield_all_subchunks_for_each_song_per_batch(self):
        for i in range(0, self.dataset.get_song_nr()):
            data, labels = self.dataset.get_song_chunks(i)
            idx = [i for _ in range(0, len(data))]
            batch = Batch(np.array(data), np.array(labels), np.array(idx))
            yield batch

    def __reset(self):
        if self.shuffle and self.dataset.subset == Subset.TRAINING:
            self.__shuffle_indices()
        self.index = 0
        self.cache = {}
        self.prefetch_index = 0
        self._prefetch_next_batch()

    def __del__(self):
        try:
            # Stop each worker by passing None to its index queue
            for i, w in enumerate(self.workers):
                self.index_queues[i].put(None)
                w.join(timeout=5.0)
            for q in self.index_queues:  # close all queues
                q.cancel_join_thread()
                q.close()
            self.output_queue.cancel_join_thread()
            self.output_queue.close()
        finally:
            for w in self.workers:
                if w.is_alive():  # manually terminate worker if all else fails
                    w.terminate()

    def _get_next_batch_item(self):
        if self.num_workers is None:
            return self._fetch_next_item_from_dataset()
        else:
            return self._fetch_next_item_from_workers()

    def _fetch_next_item_from_dataset(self):
        data, label = self.dataset[self.indices[self.index]]
        self.index += 1
        return data, label, self.indices[self.index - 1]

    def _fetch_next_item_from_workers(self):
        self._prefetch_next_batch()
        if self.indices[self.index] in self.cache:
            item_data, item_label = self.cache[self.indices[self.index]]
            del self.cache[self.indices[self.index]]
        else:
            while True:
                try:
                    idx, sample, label = self.output_queue.get(timeout=0)
                except queue.Empty:  # output queue empty, keep trying
                    continue
                if idx == self.indices[self.index]:  # found our item, ready to return
                    item_data, item_label = sample, label
                    break
                else:  # item isn't the one we want, cache for later
                    self.cache[idx] = sample, label
        self.index += 1
        return item_data, item_label, self.indices[self.index - 1]

    def _prefetch_worker_fn(self, index_queue, output_data_queue):
        while True:
            try:
                idx = index_queue.get(timeout=0)
            except queue.Empty:
                continue
            if idx is None:
                break
            data, label = self.dataset[idx]
            output_data_queue.put((idx, data, label))

    def __shuffle_indices(self):
        random.shuffle(self.indices)

    def _prefetch_next_batch(self):
        while (
                self.num_workers is not None
                and self.prefetch_index < len(self.dataset)
                and self.prefetch_index
                < self.index + 2 * self.num_workers * self.batch_size
        ):
            # if the prefetch_index hasn't reached the end of the dataset
            # and it is not 2 batches ahead, add indexes to the index queues
            # self.index_queues[next(self.worker_cycle)].put(self.prefetch_index)
            self.index_queue.put(self.indices[self.prefetch_index])
            self.prefetch_index += 1
