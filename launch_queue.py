# from run_configs.mtg_jamendo.dil_cnn import dil_cnn_configs
# from run_configs.mtg_jamendo.musicnn import musicnn_configs
# from run_configs.mtg_jamendo.resnet import resnet_configs
# from run_configs.mtg_jamendo.senet import senet_configs
# from run_configs.mtg_jamendo.vgg16 import vgg16_configs
import math

import flask

import env
from run_configs.magna_tag_a_tune.dil_cnn import dil_cnn_configs
from run_configs.magna_tag_a_tune.musicnn import musicnn_configs
from run_configs.magna_tag_a_tune.resnet import resnet_configs
from run_configs.magna_tag_a_tune.senet import senet_configs
from run_configs.magna_tag_a_tune.vgg16 import vgg16_configs
from flask import Flask, json

seeds_magna = [34958503, 1234040, 300055988, 122683773, 33444530, 935834894, 3494912]
queue = []
configs = [vgg16_configs, senet_configs, resnet_configs, musicnn_configs, dil_cnn_configs]

app = Flask(__name__)


def get_next_config():
    f = open('queue_state.txt', 'r')
    count = int(f.readline())
    if (count < len(seeds_magna) * 25):
        seed = seeds_magna[math.floor(count / 25)]
        config = configs[math.floor((count % 25) / 5)]
        config = config[count % 5]
        config['seed'] = seed
        f.close()
        f = open('queue_state.txt', 'w')
        f.write(str(count + 1))
        f.close()
        return config
    else:
        return False

@app.route('/reload', methods=['GET'])
def reload():
    key = flask.request.headers.get("QUERYKEY")
    if (key == env.QUEUE_KEY):
        f = open ('queue_state.txt', 'w')
        f.write('0')
        f.close()
        return 'Reloaded successfully'
    else:
        return "Wrong key", 401

@app.route('/queue', methods=['GET'])
def get_companies():
    key = flask.request.headers.get("QUERYKEY")
    if (key == env.QUEUE_KEY):
        conf = json.dumps(get_next_config())
        print(f'get next config: {conf}')
        return conf
    else:
        return "Wrong key", 401


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8085)
