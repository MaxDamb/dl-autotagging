import os

import pandas as pd
from tqdm import tqdm
import numpy as np
import tensorflow as tf

from batch_generator import BatchGenerator
from preprocessing import Dataset

categories = ['genre', 'instrument', 'mood']


class EvaluationResult:
    def __init__(self, model: tf.keras.Model, batch_gen: BatchGenerator, dataset: Dataset,
                 preprocessing: str, model_name: str):
        self.model = model
        self.batch_gen = batch_gen
        self.preprocessing = preprocessing
        self.model_name = model_name
        self.result = {}
        self.dataset = dataset
        self.evaluate_model()
        self.reset_metrics()

    def reset_metrics(self):
        self.m_roc_all = tf.keras.metrics.AUC(multi_label=True, curve='ROC')
        self.m_roc_genre = tf.keras.metrics.AUC(multi_label=True, curve='ROC')
        self.m_roc_instrument = tf.keras.metrics.AUC(multi_label=True, curve='ROC')
        self.m_roc_mood = tf.keras.metrics.AUC(multi_label=True, curve='ROC')
        self.m_pr_all = tf.keras.metrics.AUC(multi_label=True, curve='PR')
        self.m_pr_genre = tf.keras.metrics.AUC(multi_label=True, curve='PR')
        self.m_pr_instrument = tf.keras.metrics.AUC(multi_label=True, curve='PR')
        self.m_pr_mood = tf.keras.metrics.AUC(multi_label=True, curve='PR')
        self.cat_metrics = {
            'genre': {
                'PR': self.m_pr_genre,
                'ROC': self.m_roc_genre
            },
            'instrument': {
                'PR': self.m_pr_instrument,
                'ROC': self.m_roc_instrument
            },
            'mood': {
                'PR': self.m_pr_mood,
                'ROC': self.m_roc_mood
            }
        }

    def get_all_categories_results(self) -> (float, float):
        return self.result['all']['ROC'], self.result['all']['PR']

    def get_cat_results(self, category) -> (float, float):
        return self.result[category]['ROC'], self.result[category]['PR']

    def evaluate_model(self):
        self.reset_metrics()
        for batch in tqdm(self.batch_gen):
            images, labels = batch.data, batch.label
            images = tf.convert_to_tensor(images, dtype=tf.float16)
            prediction = self.model.predict(images)
            if self.dataset.chunk_lvl:
                prediction = [tf.math.reduce_mean(prediction, axis=0)]
                labels = [tf.convert_to_tensor(labels[0], dtype=tf.int32)]

            else:
                prediction = [tf.convert_to_tensor(val) for val in prediction]
                labels = [tf.convert_to_tensor(val, dtype=tf.int32) for val in labels]
            self.__calc_roc_and_pr(labels, prediction)


    def __str__(self):
        return self.result.__str__()

    def save_results(self):
        df = pd.DataFrame.from_dict(self.result)
        if not os.path.exists('results'):
            os.mkdir('results')
        if not os.path.exists(os.path.join('results', self.dataset.dataset_name)):
            os.mkdir(os.path.join('results', self.dataset.dataset_name))
        if not os.path.exists(os.path.join('results', self.dataset.dataset_name, self.preprocessing)):
            os.mkdir(os.path.join('results', self.dataset.dataset_name, self.preprocessing))
        df.to_csv(os.path.join('results', self.dataset.dataset_name, self.preprocessing, f'{self.model_name}.csv'))

    def __calc_roc_and_pr(self, labels: list, preds: list):
        preds = np.array(preds)
        labels = np.array(labels)

        # Calculating ROC-AUC
        self.m_roc_all.update_state(labels, preds)
        # Calculating PR-AUC
        self.m_pr_all.update_state(labels, preds)
        self.result['all'] = {'ROC': self.m_roc_all.result().numpy(), 'PR': self.m_pr_all.result().numpy()}
        for cat in categories:
            cat_indices = self.__get_label_indices_for_category(cat)
            cat_labels = np.array([i.take(cat_indices) for i in labels])
            cat_pred = np.array([i.take(cat_indices) for i in preds])
            m_roc = self.cat_metrics[cat]['ROC']
            m_pr = self.cat_metrics[cat]['PR']
            m_roc.update_state(cat_labels, cat_pred)
            m_pr.update_state(cat_labels, cat_pred)
            self.result[cat] = {'ROC': m_roc.result().numpy(), 'PR': m_pr.result().numpy()}

    def __get_label_indices_for_category(self, category: str) -> list:
        output = []
        label_categories = pd.read_csv(os.path.join('datasets', self.dataset.dataset_name, 'audio_labels', 'label_categories.csv'))
        for idx, cat in enumerate(label_categories['category']):
            if cat == category:
                output.append(idx)
        return output
